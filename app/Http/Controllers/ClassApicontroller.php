<?php

namespace App\Http\Controllers;


use App\User;
use Illuminate\Http\Request;
use App\Classes;

class ClassApicontroller extends Controller
{
    public $userModel = '';
    public $classModel = '';
    public $status = [
        'status' => '',
        'message' => '',
        'data' => '',
    ];
    public function __construct(User $user, Classes $classData)
    {
        $this->userModel = $user;
        $this->classModel = $classData;
    }

    /*
     *
     *   Api for save Class Informations
     *
     */
    public function saveClass(Request $request)
    {
        
        if (!empty($request->all())) {
            $this->status['status'] = true;
            $this->status['message'] = 'data save successfully';
            $findRecord = $this->classModel->find($request->id);
            //dd($findRecord);
            if($findRecord['attributes']){
                $findRecord['attributes'] = $request->all();
                $s = $findRecord->save();
            }else{
                $s = new Classes($request->all());
                $s->save();
            }
            
            if ($s) {
                $this->status['data'] = $s;
            } else {
                $this->status['data'] = 0;
            }
            return $this->status;
        } else {
            $this->status['status'] = fasle;
            $this->status['message'] = 'Invalid data';
            return $this->status;
        }
    }

    /*
    *
    *   For get all class details
    *
    */
    public function getallClass(){
        $classData = $this->classModel->select('*')->get();
        $allRecords = [];
        foreach ($classData as $d) {
           if(!empty($d['attributes'])){
                $allRecords[] = $d['attributes'];
           }else{
                $allRecords = [];
           }
        }
        if(!empty($allRecords)){
            $this->status['status'] = true;
            $this->status['message'] = 'Data Found';
            $this->status['data'] = $allRecords;
            return $this->status;
        }else{
            $this->status['status'] = false;
            $this->status['message'] = 'Data Not Found';
            $this->status['data'] = [];
            return $this->status;
        }
    }


    /*
    *
    *  Get Single Classes Records
    *
    */
    public function getsingleClass($id){
		$decodeId = base64_decode($id);
        if(!empty($decodeId)){
            $SingleData = $this->classModel->select('*')->where("id", $decodeId)->first();
			
            if(!empty($SingleData['attributes'])){
                $this->status['status'] = true;
                $this->status['message'] = 'Data Found';
                $this->status['data'] = $SingleData['attributes'];
            }else{
                $this->status['status'] = true;
                $this->status['message'] = 'No Record Found';
                $this->status['data'] = [];
            }
        }else{
            $this->status['status'] = false;
            $this->status['message'] = 'Data Not Found';
            $this->status['data'] = [];
        }

        return $this->status;
    }

}
