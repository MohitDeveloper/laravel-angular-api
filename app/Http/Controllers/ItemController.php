<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\User;

class ItemController extends Controller
{
  public $userModel = '';
  public function __construct( User $user)
  {
    $this->userModel = $user;
  }
   /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		/* echo "<pre>....";
		print_r($request->get('name'));die; */
        $item = new Item([
          'name' => $request->get('name'),
          'price' => $request->get('price')
        ]);
        $item->save();
        return response()->json('Successfully added');
    }

     /*
    *
    * User List API
    * 
    * */
    public function userList(){
      $user = new User();
      $data = $user->select("*")->get();
      $allUserData = '';
      $status = '';
      if (!empty($data)) {
          foreach ($data as $record) {
              $allUserData[] = $record['attributes'];
          }
          $status = [
            "success" => true,
            'message' => 'Data Found',
            'data' => $allUserData
          ];
          
      } else{
        $status = [
          "success" => fasle,
          'message' => 'Data Not Found',
          'data' => []
        ];
      }
      return $status;
  }


  /*
  *
  *  For get Single user 
  *
  */
  public function getSingleUser($id){
	  $encodId = base64_decode($id);
    if(!empty($encodId)){
      $getuser = $this->userModel->select('*')->where(['id' => $encodId])->first();

      if(!empty($getuser['attributes'])){
        return $status = [
          "success" => true,
            'message' => 'Data Found',
            'data' => $getuser['attributes']
        ];
      }else{
        return $status = [
          "success" => true,
            'message' => 'No Record Found',
            'data' => []
        ];
      }
      
    }else{
      return $status = [
        "success" => fasle,
          'message' => 'Data Not Found',
          'data' => []
      ];
    }
  }
  
  /*
  *
  *Api for update uesr
  *  
  */
  public function updateUser(Request $request){
	  if(!empty($request)){
      $dataUpdate = [
        'email' => $request->email,
        'name' => $request->name,
      ];
      $UpdateRecordValue = $this->userModel->where("id" ,$request->id)->update($dataUpdate);
      if($UpdateRecordValue){
        return $status = [
          "success" => true,
          'message' => 'Successfully Update',
          'data' => $UpdateRecordValue
        ]; 
      }else{
        return $status = [
          "success" => true,
          'message' => 'Not Update',
          'data' => 0
        ]; 
      }
	  	 
	  }else{
		  return $status = [
        "success" => false,
        'message' => 'Data Not Found',
        'data' => []
      ];  
	  }
  }
}
