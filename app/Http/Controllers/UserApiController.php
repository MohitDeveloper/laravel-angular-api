<?php

namespace App\Http\Controllers;

use App\Student;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;

class UserApiController extends Controller
{
    public $userModel = '';
    public $studentModel = '';
    public $status = [
        'status' => '',
        'message' => '',
        'data' => '',
    ];
    public function __construct(User $user, Student $studentData)
    {
        $this->userModel = $user;
        $this->studentModel = $studentData;
    }

    /*
     *
     *   Api for save Student Informations
     *
     */
    public function saveStudent(Request $request)
    {
        
        if (!empty($request->all())) {
            $this->status['status'] = true;
            $this->status['message'] = 'data save successfully';
            $findRecord = $this->studentModel->find($request->id);
            //dd($findRecord);
            if($findRecord['attributes']){
                $findRecord['attributes'] = $request->all();
                $s = $findRecord->save();
            }else{
                $s = new Student($request->all());
                $s->save();
            }
            
            if ($s) {
                $this->status['data'] = $s;
            } else {
                $this->status['data'] = 0;
            }
            return $this->status;
        } else {
            $this->status['status'] = fasle;
            $this->status['message'] = 'Invalid data';
            return $this->status;
        }
    }

    /*
    *
    *   For get all student details
    *
    */
    public function getallStudent(){
        $studentData = $this->studentModel->select('*')->get();
        $allRecords = [];
        foreach ($studentData as $d) {
           if(!empty($d['attributes'])){
                $allRecords[] = $d['attributes'];
           }else{
                $allRecords = [];
           }
        }
        if(!empty($allRecords)){
            $this->status['status'] = true;
            $this->status['message'] = 'Data Found';
            $this->status['data'] = $allRecords;
            //return response($this->status);
            //return response()->json($this->status, 200);
            return json_encode($this->status);
        }else{
            $this->status['status'] = false;
            $this->status['message'] = 'Data Not Found';
            $this->status['data'] = [];
            return response()->json($this->status, 200);
        }
    }


    /*
    *
    *  Get Single Student Records
    *
    */
    public function getSingleStudent($id){
		$decodeId = base64_decode($id);
        if(!empty($decodeId)){
            $SingleData = $this->studentModel->select('*')->where("id", $decodeId)->first();
			
            if(!empty($SingleData['attributes'])){
                $this->status['status'] = true;
                $this->status['message'] = 'Data Found';
                $this->status['data'] = $SingleData['attributes'];
            }else{
                $this->status['status'] = true;
                $this->status['message'] = 'No Record Found';
                $this->status['data'] = [];
            }
        }else{
            $this->status['status'] = false;
            $this->status['message'] = 'Data Not Found';
            $this->status['data'] = [];
        }

        return $this->status;
    }

}
