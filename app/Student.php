<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
   
    protected  $tables = "students";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'student_name', 'father_name', 'class', 'fees', 'mobile_no', 'address', 'email'
    ];

    
}
