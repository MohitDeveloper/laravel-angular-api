<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('items', 'ItemController@store');
Route::post('items', 'ItemController@store');
Route::get("getUser", 'ItemController@userList')->name('User-Data');
Route::get("getSingleUser/{id}", 'ItemController@getSingleUser')->name('User-Single-Data');
//Route::get("updateUser/{id}", 'ItemController@updateUser')->name('User-Update-Data');
Route::post("updateUser", 'ItemController@updateUser')->name('User-Update-Data');
Route::post("saveStudent", "UserApiController@saveStudent")->name("Student-Data-Save");
Route::get("getallStudent", "UserApiController@getallStudent")->name("All-Student-Details");
Route::get("getSingleStudent/{id}", "UserApiController@getSingleStudent")->name("Single-Student-Detail");


Route::post('saveClass', 'ClassApiController@saveClass')->name('Save-Class');
Route::get('getallClass', 'ClassApiController@getallClass')->name('Get-All-Class');
Route::get('getsingleClass/{id}', 'ClassApiController@getsingleClass')->name('Get-Single-Class');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


